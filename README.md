# Bismuth Platform
### Bismuth is multi-function utilities for teach or hangouts Platform
##### Will write in C with OpenCL, OpenGL/Vulkan

It is design to Peer-to-Peer **Video.Audio** streaming as a confereence room/classroom.
Accomplish by
| function | how |
| :------: | :---: |
| Graphic User Interface | OpenGL or Vulkan |
| Peer to Peer | STUN |
| Video/Audeo en/decode | ffmpeg/OpenCL |
| Account Data | undetemined |
| Users' IP when calling | linked-list |

Compile: CMake

